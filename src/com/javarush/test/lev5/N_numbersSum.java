package com.javarush.test.lev5;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Артем on 01.02.2016.
 */
public class N_numbersSum {


/* Вводить с клавиатуры числа и считать их сумму
Вводить с клавиатуры числа и считать их сумму, пока пользователь не введёт слово «сумма». Вывести на экран полученную сумму.
*/



        public static void main(String[] args) throws Exception
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int sum = 0;

            while (true)
            {String s = reader.readLine();
                if (s.equals("сумма"))break;
                else sum += Integer.parseInt(s);
            }System.out.println(sum);
        }
    }


