package com.javarush.test.lev5;

/**
 * Created by Артем on 31.01.2016.
 */
public class Circle {
    private int centerX;
    private int centerY;
    private int radius;
    private int width;
    String color;

    public Circle(int centerX, int centerY, int radius, int width, String color ){
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.width = width;
        this.color = color;

    }
}
