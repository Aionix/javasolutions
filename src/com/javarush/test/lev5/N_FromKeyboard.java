package com.javarush.test.lev5;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Артем on 01.02.2016.
 */
public class N_FromKeyboard {

    /* Задача по алгоритмам
Написать программу, которая:
1. вводит с консоли число N > 0
2. потом вводит N чисел с консоли
3. выводит на экран максимальное из введенных N чисел.
*/
        public static void main(String[] args) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int n = Integer.parseInt(reader.readLine());
            int maximum = 0;
            for (int i = 0; i < n; i++) {
                int tempo = Integer.parseInt(reader.readLine());
                if (i == 0) maximum = tempo;
                else if (tempo > maximum)
                    maximum = tempo;
                System.out.println(maximum);
            }
        }}