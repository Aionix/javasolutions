package com.javarush.test.lev5;

/**
 * Created by Артем on 26.02.2016.
 */
public class JustTest
{
    int a = 5;
    int sum()
    {
        return a;
    }
}

class Run extends JustTest
{
    int a = 10;
    int sum() {return a; }

    public static void main(String[] args)
    {
        Run run = new Run();
        JustTest base = new Run();

        System.out.println(run.a + " " + base.a);
        System.out.println(run.sum() + " " + base.sum());

    }}
