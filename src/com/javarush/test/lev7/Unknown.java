package com.javarush.test.lev7;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Артем on 08.02.2016.
 *
 1. Введи с клавиатуры 10 слов в список строк.
 2. Метод doubleValues должен удваивать слова по принципу a,b,c -> a,a,b,b,c,c.
 3. Используя цикл for выведи результат на экран, каждое значение с новой строки.
 */
public class Unknown {
    public static void main(String[] args) throws Exception {
        //Считать строки с консоли и объявить ArrayList list тут
        ArrayList<String> list = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 3; i++) {
            list.add(reader.readLine());
        }

        ArrayList<String> result =  doubleValues(list);
        for (int k = 0; k < result.size()-1; k++) {
            System.out.println(result.get(k));
        }
        //Вывести на экран result
    }

    public static ArrayList<String> doubleValues(ArrayList<String> list2) {
        for (int i = 0; i < list2.size(); i++) {
            list2.add(i, list2.get(i));
            i++;
        }return list2;
    }
}





