package com.javarush.test.lev7;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Артем on 10.02.2016.
 */
public class DifferentDivisions {
    public static void main(String[] args) throws Exception {
        ArrayList<Integer> list =           new ArrayList<Integer>();
        ArrayList<Integer> dividedByThree = new ArrayList<Integer>();
        ArrayList<Integer> dividedByTwo =   new ArrayList<Integer>();
        ArrayList<Integer> dividedOther =   new ArrayList<Integer>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 10; i++) {
            list.add(Integer.parseInt(reader.readLine()));
        }
        for (int j = 0; j < list.size(); j++) {
            if (list.get(j) % 2 == 0 && list.get(j)%3 == 0){
                dividedByTwo.add(list.get(j));
                dividedByThree.add(list.get(j));}
            else if (list.get(j) % 3 == 0){
                dividedByThree.add(list.get(j));}
            else if (list.get(j) % 2 == 0){
                dividedByTwo.add(list.get(j));}

            else dividedOther.add(list.get(j));
        }
        // printList(dividedByThree);
        //  printList(dividedByTwo);
        printList(dividedOther);
    }
    public static void printList(ArrayList<Integer> someList){
        for (int c = 0; c < someList.size(); c ++){
            System.out.println(someList.get(c));
        }

    }
}
