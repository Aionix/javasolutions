package com.javarush.test.lev8;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Артем on 02.03.2016.
 */
public class MapNameSurname {

        public static Map<String, String> createMap()
        {
            Map<String, String> map = new HashMap<>();
            map.put("F",      "Some");
            map.put("Fa",     "Kolia");
            map.put("Fam",    "Kolia");
            map.put("Fami",   "Some");
            map.put("Famil",  "Lal");
            map.put("Family",  "Lol");
            map.put("Name",    "lalala");
            map.put("Nam",      "Kol");
            map.put("Names",    "word");
            map.put("N",       "ma");
            return map;
        }

        public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
        {   int a = 0;
            for (Map.Entry<String, String> pair : map.entrySet()){
                String name1 = pair.getValue();
                if(name.)a++;
            }return a;
        }

        public static int getCountTheSameLastName(HashMap<String, String> map, String lastName)
        {
            int b = 0;
            for (Map.Entry<String, String> pair : map.entrySet()){
                String lastName1 = pair.getKey();
                if(lastName1.equals(lastName))b++;
            }return b;

        }
    }

