package com.javarush.test;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* Коллекция HashMap из котов
Есть класс Cat с полем имя (name, String).
Создать коллекцию HashMap<String, Cat>.
Добавить в коллекцию 10 котов, в качестве ключа использовать имя кота.
Вывести результат на экран, каждый элемент с новой строки.
*/

public class SolutionToClearOut
{
    public static void main(String[] args) throws Exception
    {
        String[] cats = new String[] {"васька", "мурка", "дымка", "рыжик", "серый", "снежок", "босс", "борис", "визя", "гарфи"};

        HashMap<String, Cat> map1 = addCatsToMap(cats);

        for (Map.Entry<String, Cat> pair : map1.entrySet())
        {
            System.out.println(pair.getKey() + " - " + pair.getValue());
        }
    }


    public static HashMap<String, Cat> addCatsToMap(String[] cats2)
    {
        //напишите тут ваш код
        HashMap<String, Cat> cat1 = new HashMap<String, Cat>();
        for (int i = 0; i < cats2.length; i++ ){
            cat1.put(cats2[i], new Cat(cats2[i]));
        }return cat1;

    }


    public static class Cat
    {
        String name;

        public Cat(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return name != null ? name.toUpperCase() : null;
        }
    }
}
